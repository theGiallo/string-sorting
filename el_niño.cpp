#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
typedef uint8_t  u8;
typedef uint32_t u32;
typedef uint64_t u64;
typedef  int32_t s32;
#define U32_MAX UINT32_MAX
#define U64_MAX UINT64_MAX
#define ARRAYLENGTH(a) ( sizeof( a ) / sizeof( (a)[0] ) )

#define MIN(a,b) ((a)<(b)?(a):(b))

#define INDENT_STRING "   "
#define PRINT_INDENTATION(ind)  \
for ( u32 i = 0; i < ind; ++i ) \
{                               \
	printf( INDENT_STRING );    \
}                               \

u64
ns_time()
{
	u64 ret;

	timespec t;
	int r = clock_gettime( CLOCK_REALTIME, &t );
	if ( r == -1 )
	{
		fprintf( stderr, "error: clock_gettime\n" );
	}
	ret = t.tv_sec * 1e9 + t.tv_nsec;

	return ret;
}

enum
Sort_Order : u8
{
	SORT_ASCENDANTLY,
	SORT_DESCENDANTLY
};

#define PRINT_VERBOSE 0
bool
sort_lexicographically_radix( char const** strings, u32 count,
                              char const** tmp,
                              Sort_Order sort_order = SORT_ASCENDANTLY,
                              u32 * data     = NULL,
                              u32 * data_tmp = NULL,
                              u32 * lengths_strings = NULL,
                              u32 * lengths_tmp     = NULL,
                              u32 char_idx = 0 )
{
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		u32 lengths_t[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix( strings, count, tmp,
		                                     sort_order,
		                                     data, data_tmp,
		                                     lengths_s, lengths_t, char_idx );
	}

	bool solution_is_in_tmp = true;

#if PRINT_VERBOSE
	PRINT_INDENTATION(char_idx);
	printf( "char index %u\n", char_idx );
	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( "strings:\n" );
	for ( u32 i = 0; i < count; ++i )
	{
		PRINT_INDENTATION(char_idx);
		printf( "[%2u]( %u ) %s\n", i, data[i], strings[i] );
	}

	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( "tmp:\n" );
	for ( u32 i = 0; i < count; ++i )
	{
		PRINT_INDENTATION(char_idx);
		printf( "[%2u]( %u ) %s\n", i, data_tmp[i], tmp[i] );
	}
	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( "--------------------------------------------------------------------------------\n" );
#endif

	u32 bins_count[256] = {};
	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
#if 0
		printf( "s_idx = %u\n", s_idx );
		printf( "dest  = %u\n", dest  );
		printf( "c     = %u\n", c     );
		printf( "\n" );
		fflush( stdout );
#endif
		tmp[dest] = strings[s_idx];
		lengths_tmp[dest] = lengths_strings[s_idx];
		if ( data )
		{
			data_tmp[dest] = data[s_idx];
		}
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_strings =
		sort_lexicographically_radix( tmp + first, bin_count, strings + first,
		                              sort_order,
		                              data_tmp + first, data + first,
		                              lengths_strings + first, lengths_tmp + first,
		                              char_idx+1 );
		if ( res_is_in_strings )
		{
			memcpy( tmp + first, strings + first, bin_count * sizeof( tmp[0] ) );
			memcpy( lengths_tmp + first, lengths_strings + first,
			        bin_count * sizeof( lengths_tmp[0] ) );
			if ( data )
			{
				memcpy( data_tmp + first, data + first,
				        bin_count * sizeof( data[0] ) );
			}
		}
	}

#if PRINT_VERBOSE
	PRINT_INDENTATION(char_idx);
	printf( "char index %u\n", char_idx );
	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( " - sorted -\n" );
	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( "strings:\n" );
	for ( u32 i = 0; i < count; ++i )
	{
		PRINT_INDENTATION(char_idx);
		printf( "[%2u]( %u ) %s\n", i, data[i], strings[i] );
	}

	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( "tmp:\n" );
	for ( u32 i = 0; i < count; ++i )
	{
		PRINT_INDENTATION(char_idx);
		printf( "[%2u]( %u ) %s\n", i, data_tmp[i], tmp[i] );
	}
	printf( "\n" );
	PRINT_INDENTATION(char_idx);
	printf( "--------------------------------------------------------------------------------\n" );
#endif

	solution_is_in_tmp = false;
	memcpy( strings, tmp, count * sizeof( tmp[0] ) );
	if ( data )
	{
		memcpy( data, data_tmp, count * sizeof( data[0] ) );
	}
	return solution_is_in_tmp;
}

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
s32
string_min( const char * string0, const char * string1 )
{
	s32 ret = 0;
	const u8 * us0 = (const u8 *) string0;
	const u8 * us1 = (const u8 *) string1;
	for ( u32 i = 0; i < U32_MAX; ++i )
	{
		if ( us0[i] && us1[i] )
		{
			if ( us0[i] == us1[i] )
			{
				continue;
			}// else
			if ( us0[i] < us1[i] )
			{
				ret = -1;
			} else
			//if ( us0[i] > us1[i] )
			{
				ret = 1;
			}
		} else
		if ( us0[i] && !us1[i] )
		{
			ret = 1;
		} else
		if ( !us0[i] && us1[i] )
		{
			ret = -1;
		} else
		{
			ret = 0;
		}
		break;
	}

	return ret;
}

void
sort_lexicographically_insertion( const char ** strings, u32 count,
                                  u32 * data = NULL,
                                  Sort_Order sort_order = SORT_ASCENDANTLY )
{
	for ( u32 i = 0; i < count; ++i )
	{
#if 0
		if ( i % (count/10) == 0 )
		{
			printf( "%u%%\n", i * 10 / (count/10) );
		}
#endif
		const char * s = strings[i];
		u32 d;
		if ( data )
		{
			d = data[i];
		}
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[j] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				strings[j+1] = strings[j];
				if ( data )
				{
					data[j+1] = data[j];
				}
			} else
			{
				break;
			}
		}
		strings[j+1] = s;
		if ( data )
		{
			data[j+1] = d;
		}
	}
}

#define COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX 150
bool
sort_lexicographically_radix_insertion( char const** strings, u32 count,
                                        char const** tmp,
                                        Sort_Order sort_order = SORT_ASCENDANTLY,
                                        u32 * data     = NULL,
                                        u32 * data_tmp = NULL,
                                        u32 * lengths_strings = NULL,
                                        u32 * lengths_tmp     = NULL,
                                        u32 char_idx = 0 )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion( strings, count, data, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		u32 lengths_t[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion( strings, count, tmp,
		                                               sort_order,
		                                               data, data_tmp,
		                                               lengths_s, lengths_t,
		                                               char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		tmp[dest] = strings[s_idx];
		lengths_tmp[dest] = lengths_strings[s_idx];
		if ( data )
		{
			data_tmp[dest] = data[s_idx];
		}
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_strings;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_strings =
			sort_lexicographically_radix_insertion( tmp + first, bin_count, strings + first,
			                                        sort_order,
			                                        data_tmp + first, data + first,
			                                        lengths_strings + first, lengths_tmp + first,
			                                        char_idx+1 );
		} else
		{
			res_is_in_strings = false;
			sort_lexicographically_insertion( tmp + first, bin_count, data_tmp + first, sort_order );
		}
		if ( res_is_in_strings )
		{
			memcpy( tmp + first, strings + first, bin_count * sizeof( tmp[0] ) );
			memcpy( lengths_tmp + first, lengths_strings + first,
			        bin_count * sizeof( lengths_tmp[0] ) );
			if ( data )
			{
				memcpy( data_tmp + first, data + first,
				        bin_count * sizeof( data[0] ) );
			}
		}
	}

	solution_is_in_tmp = false;
	memcpy( strings, tmp, count * sizeof( tmp[0] ) );
	if ( data )
	{
		memcpy( data, data_tmp, count * sizeof( data[0] ) );
	}
	return solution_is_in_tmp;
}


void
sort_lexicographically( char const** strings, u32 count,
                        char const** tmp,
                        Sort_Order sort_order,
                        u32 * data     = NULL,
                        u32 * data_tmp = NULL,
                        u32 * lengths_strings = NULL,
                        u32 * lengths_tmp     = NULL )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion( strings, count, data, sort_order );
	}
	sort_lexicographically_radix_insertion( strings, count, tmp,
	                                        sort_order,
	                                        data, data_tmp,
	                                        lengths_strings, lengths_tmp );
}

uint64_t
xorshift128plus( uint64_t seeds[2] )
{
	uint64_t       x = seeds[0];
	uint64_t const y = seeds[1];
	seeds[0] = y;
	x ^= x << 23; // a
	x ^= x >> 17; // b
	x ^= y ^ (y >> 26); // c
	seeds[1] = x;
	return x + y;
}

double
randd_01( uint64_t seeds[2] )
{
	uint64_t rnd = xorshift128plus( seeds );
	double ret = ( rnd / (double)U64_MAX );
	return ret;
}

double
randd_between( uint64_t seeds[2], double min, double max )
{
	uint64_t rnd = xorshift128plus( seeds );
	double ret = ( rnd / (double)U64_MAX ) * ( max - min ) + min;
	return ret;
}
u32
generate_string( u8 * mem, u32 byte_size,
                 u32 min_length, u32 max_length, u64 seeds[2] )
{
	u32 length = round( randd_between( seeds, min_length, max_length ) );
	length = MIN( length, byte_size );
	for ( u32 i = 0; i < length; ++i )
	{
		mem[i] = round( randd_between( seeds, 1, 255 ) );
	}
	mem[length] = 0;

	return length;
}

int main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;

#if 0
	// NOTE(theGiallo): This shows the encoding of the __FILE__ string (UTF-8 with clang++-3.9)
	printf( "file:\n%s\n", __FILE__ );
	int l = 0;
	const char * file = __FILE__;
	while( file[l] ) ++l;
	printf( "length = %d\n", l );
	for ( int i = 0; i < l; ++i )
	{
		unsigned char c = ((unsigned char*)file)[i];
		printf( "[%d] %c (%#x)(", i, file[i], (unsigned)c );
		for ( int j = 7; j; --j )
		{
			printf( c & ( 1<<j ) ? "1" : "0" );
		}
		printf( ")\n" );
	}
#endif

#if 0
{
	const char * const strings[] = {
		"el_nino.cpp",
		__FILE__,
		"el_nino.cpp",
		"el_nimo.cpp",
		"el_nimo.cpp~",
		"el_nimo.cpp",
	};

	assert( string_min( strings[4], strings[0] ) == -1 );
	assert( string_min( strings[1], strings[0] ) == 1 );

	for ( u32 i = 0; i < 2; ++i )
	{
		Sort_Order sort_order = (Sort_Order)i;
		u32 indices_tmp[ ARRAYLENGTH( strings )];
		u32 indices[ ARRAYLENGTH( strings )];
		for ( u32 i = 0; i < ARRAYLENGTH(strings); ++i )
		{
			indices[i] = i;
		}
		const char * strings_to_sort[ARRAYLENGTH( strings )];
		memcpy( strings_to_sort, strings, sizeof( strings ) );
		u64 start_time = ns_time();
		const char * tmp[ARRAYLENGTH( strings )] = {};
		sort_lexicographically( strings_to_sort, ARRAYLENGTH( strings ), tmp,
		                        sort_order, indices, indices_tmp );
		u64 end_time = ns_time();
		u64 ns_dur = end_time - start_time;
		printf( "sort_lexicographically took %lu ns ( %e ms )\n", ns_dur, ns_dur * 1e-6 );
		for ( u32 i = 0; i < ARRAYLENGTH(strings); ++i )
		{
			printf( "[%2u] ( %u ) %s\n", i, indices[i], strings_to_sort[i] );
		}
	}
	for ( u32 i = 0; i < 2; ++i )
	{
		Sort_Order sort_order = (Sort_Order)i;
		u32 indices_tmp[ ARRAYLENGTH( strings )];
		u32 indices[ ARRAYLENGTH( strings )];
		for ( u32 i = 0; i < ARRAYLENGTH(strings); ++i )
		{
			indices[i] = i;
		}
		const char * strings_to_sort[ARRAYLENGTH( strings )];
		memcpy( strings_to_sort, strings, sizeof( strings ) );
		u64 start_time = ns_time();
		const char * tmp[ARRAYLENGTH( strings )] = {};
		sort_lexicographically_radix( strings_to_sort, ARRAYLENGTH( strings ), tmp,
		                              sort_order, indices, indices_tmp );
		u64 end_time = ns_time();
		u64 ns_dur = end_time - start_time;
		printf( "sort_lexicographically_radix took %lu ns ( %e ms )\n", ns_dur, ns_dur * 1e-6 );
		for ( u32 i = 0; i < ARRAYLENGTH(strings); ++i )
		{
			printf( "[%2u] ( %u ) %s\n", i, indices[i], strings_to_sort[i] );
		}
	}
	for ( u32 i = 0; i < 2; ++i )
	{
		Sort_Order sort_order = (Sort_Order)i;
		u32 indices[ ARRAYLENGTH( strings )];
		for ( u32 i = 0; i < ARRAYLENGTH(strings); ++i )
		{
			indices[i] = i;
		}
		const char * strings_to_sort[ARRAYLENGTH( strings )];
		memcpy( strings_to_sort, strings, sizeof( strings ) );
		u64 start_time = ns_time();
		sort_lexicographically_insertion( strings_to_sort, ARRAYLENGTH( strings ), indices, sort_order );
		u64 end_time = ns_time();
		u64 ns_dur = end_time - start_time;
		printf( "sort_lexicographically_insertion took %lu ns ( %e ms )\n", ns_dur, ns_dur * 1e-6 );
		for ( u32 i = 0; i < ARRAYLENGTH(strings); ++i )
		{
			printf( "[%2u] ( %u ) %s\n", i, indices[i], strings_to_sort[i] );
		}
	}
}
#endif
#if 1
#define TEST 0

#if 0
	Sort_Order sort_order = SORT_ASCENDANTLY;
#else
	Sort_Order sort_order = SORT_DESCENDANTLY;
#endif
	u64 seeds[2] = {298348792638769817L, ns_time()};
	printf( "seeds: %lu %lu\n", seeds[0], seeds[1] );
#define TRIES_COUNT 16
#define MIN_LENGTH 5
#define MAX_LENGTH 1023
#define MEM_SIZE (1024*32*(MAX_LENGTH+1))
#define STRINGS_SIZE ((MEM_SIZE+MIN_LENGTH-1)/MIN_LENGTH)
	u8 *   mem            = (u8 *)  calloc( MEM_SIZE,     1              );
	u8 ** strings         = (u8 **) calloc( STRINGS_SIZE, sizeof( u8 * ) );
	u32 * indices         = (u32*)  calloc( STRINGS_SIZE, sizeof( u32  ) );
	u32 * strings_lengths = (u32*)  calloc( STRINGS_SIZE, sizeof( u32  ) );
	assert(mem );
	assert(strings );
	assert(indices );
	assert(strings_lengths );
	u32 strings_count = 0;
	u8 * free_mem = mem;
	u8 * ceiling = mem + MEM_SIZE;
	while ( free_mem < ceiling )
	{
		strings[strings_count] = free_mem;
		strings_lengths[strings_count] =
		   generate_string( free_mem, ceiling - free_mem, MIN_LENGTH, MAX_LENGTH, seeds );
		free_mem += strings_lengths[strings_count];
		indices[strings_count] = strings_count;
		++strings_count;
	}
	printf( "generated %u strings\n", strings_count );
	fflush( stdout );

	{
		FILE * fp = fopen( "radix_insertion_150.txt", "w" );
		assert( fp );

		char const ** strings_to_sort;
		strings_to_sort = (char const **) calloc( strings_count, sizeof( strings_to_sort[0] ) );
		char const**    tmp_mem;
		tmp_mem         = (char const **) calloc( strings_count, sizeof( tmp_mem[0] ) );
		u32   * indices_tmp;
		indices_tmp     = (u32*)          calloc( strings_count, sizeof( indices_tmp[0] ) );
		u32   * lengths_s;
		lengths_s       = (u32*)          calloc( strings_count, sizeof( lengths_s[0] ) );
		u32   * lengths_t;
		lengths_t       = (u32*)          calloc( strings_count, sizeof( lengths_t[0] ) );
		assert(strings_to_sort );
		assert(tmp_mem );
		assert(lengths_s );
		assert(lengths_t );
		for ( u32 i = 0; i < strings_count; i+=(i<1000?1:1000) )
		{
			printf( "sorting with radix insertion 150 %5u strings\n", i );
			u64 min_dur = U64_MAX;
			for ( u32 j = 0; j < TRIES_COUNT; ++j )
			{
				for ( u32 j = 0; j < i; ++j )
				{
					indices[j] = j;
				}
				memcpy( strings_to_sort, strings,         i * sizeof( strings[0]   ) );
				memcpy( lengths_s,       strings_lengths, i * sizeof( lengths_s[0] ) );
				u64 start_time = ns_time();
				sort_lexicographically( strings_to_sort, i, tmp_mem,
				                        sort_order,
				                        indices, indices_tmp,
				                        lengths_s, lengths_t );
				u64 end_time = ns_time();
				u64 ns_dur = end_time - start_time;
				if ( ns_dur < min_dur )
				{
					min_dur = ns_dur;
				}
			}
#if TEST
			for ( u32 sid = 1; sid < i; ++sid )
			{
				assert( string_min( strings_to_sort[sid-1],
				                    strings_to_sort[sid] ) ==
				        ( sort_order == SORT_ASCENDANTLY ? -1 : 1 ) ||
				        ( string_min( strings_to_sort[sid-1],
				                      strings_to_sort[sid] ) == 0 &&
				          indices[sid-1] <= indices[sid]
				        ) );
			}
#endif
			fprintf( fp, "%u %lu\n", i, min_dur );
		}
		free( strings_to_sort );
		free( tmp_mem );
		free( lengths_s );
		free( lengths_t );
		fclose( fp );
	}
	{
		FILE * fp = fopen( "radix.txt", "w" );
		assert( fp );

		char const ** strings_to_sort;
		strings_to_sort = (char const **) calloc( strings_count, sizeof( strings_to_sort[0] ) );
		char const**    tmp_mem;
		tmp_mem         = (char const **) calloc( strings_count, sizeof( tmp_mem[0] ) );
		u32   * indices_tmp;
		indices_tmp     = (u32*)          calloc( strings_count, sizeof( indices_tmp[0] ) );
		u32   * lengths_s;
		lengths_s       = (u32*)          calloc( strings_count, sizeof( lengths_s[0] ) );
		u32   * lengths_t;
		lengths_t       = (u32*)          calloc( strings_count, sizeof( lengths_t[0] ) );
		assert(strings_to_sort );
		assert(tmp_mem );
		assert(lengths_s );
		assert(lengths_t );
		for ( u32 i = 0; i < strings_count; i+=(i<1000?1:1000) )
		{
			printf( "sorting with radix %5u strings\n", i );
			u64 min_dur = U64_MAX;
			for ( u32 j = 0; j < TRIES_COUNT; ++j )
			{
				for ( u32 j = 0; j < i; ++j )
				{
					indices[j] = j;
				}
				memcpy( strings_to_sort, strings,         i * sizeof( strings[0]   ) );
				memcpy( lengths_s,       strings_lengths, i * sizeof( lengths_s[0] ) );
				u64 start_time = ns_time();
				sort_lexicographically_radix( strings_to_sort, i, tmp_mem,
				                              sort_order,
				                              indices, indices_tmp,
				                              lengths_s, lengths_t );
				u64 end_time = ns_time();
				u64 ns_dur = end_time - start_time;
				if ( ns_dur < min_dur )
				{
					min_dur = ns_dur;
				}
			}
#if TEST
			for ( u32 sid = 1; sid < i; ++sid )
			{
				assert( string_min( strings_to_sort[sid-1],
				                    strings_to_sort[sid] ) ==
				        ( sort_order == SORT_ASCENDANTLY ? -1 : 1 ) ||
				        ( string_min( strings_to_sort[sid-1],
				                      strings_to_sort[sid] ) == 0 &&
				          indices[sid-1] <= indices[sid]
				        ) );
			}
#endif
			fprintf( fp, "%u %lu\n", i, min_dur );
		}
		free( strings_to_sort );
		free( tmp_mem );
		free( lengths_s );
		free( lengths_t );
		fclose( fp );
	}
	{
		FILE * fp = fopen( "insertion.txt", "w" );
		assert( fp );

		char const ** strings_to_sort;
		strings_to_sort = (char const **) calloc( strings_count, sizeof( strings_to_sort[0] ) );
		assert(strings_to_sort );
		for ( u32 i = 0; i < strings_count; i+=(i<1000?1:1000) )
		{
			printf( "sorting with insertion %5u strings\n", i );
			u64 min_dur = U64_MAX;
			for ( u32 j = 0; j < TRIES_COUNT; ++j )
			{
				for ( u32 j = 0; j < i; ++j )
				{
					indices[j] = j;
				}
				memcpy( strings_to_sort, strings, i * sizeof( strings_to_sort[0] ) );
				u64 start_time = ns_time();
				sort_lexicographically_insertion( strings_to_sort, i, indices, sort_order );
				u64 end_time = ns_time();
				u64 ns_dur = end_time - start_time;
				if ( ns_dur < min_dur )
				{
					min_dur = ns_dur;
				}
			}
#if TEST
			for ( u32 sid = 1; sid < i; ++sid )
			{
				assert( string_min( strings_to_sort[sid-1],
				                    strings_to_sort[sid] ) ==
				        ( sort_order == SORT_ASCENDANTLY ? -1 : 1 ) ||
				        ( string_min( strings_to_sort[sid-1],
				                      strings_to_sort[sid] ) == 0 &&
				          indices[sid-1] <= indices[sid]
				        ) );
			}
#endif
			fprintf( fp, "%u %lu\n", i, min_dur );
		}
		free( strings_to_sort );
		fclose( fp );
	}
#endif

	return 0;
}
